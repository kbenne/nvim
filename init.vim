set clipboard+=unnamedplus
"set guitablabel=\[%N\]\ %t\ %M
set guitablabel=%t

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
"call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
call vundle#begin('~/.config/nvim/bundle')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'a.vim'
Plugin 'https://github.com/vim-airline/vim-airline.git'
Plugin 'mkitt/tabline.vim'
Plugin 'https://github.com/altercation/vim-colors-solarized.git'
Plugin 'kien/ctrlp.vim'
Plugin 'nvie/vim-flake8'

" All of your Plugins must be added before the following line
call vundle#end()            " required
"filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
filetype plugin on
"
" Brief help
" :PluginList          - list configured plugins
" :PluginInstall(!)    - install (update) plugins
" :PluginSearch(!) foo - search (or refresh cache first) for foo
" :PluginClean(!)      - confirm (or auto-approve) removal of unused plugins
"
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
set ignorecase
set tabstop=2
set shiftwidth=2
set softtabstop=2 
set expandtab

"let g:PyFlakeSigns = 0
"autocmd FileType python map <buffer> <F3> :call flake8#Flake8()<CR>

"command Flake8 flake8#Flake8()
command Flake8 :call flake8#Flake8()
autocmd BufWritePost *.py call flake8#Flake8()

syntax enable
set background=dark
"colorscheme solarized
